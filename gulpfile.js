var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCss = require('gulp-csso');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var browserSync = require('browser-sync');

gulp.task('css', function() {
    return gulp.src('src/sass/**/*.sass')
        .pipe(sass())
        .pipe(minifyCss())
        .pipe(gulp.dest('src/css'))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('js', function() {
    return gulp.src('src/js/**/*.js')
        .pipe(sourcemaps.init())
        .pipe(concat('scripts.min.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/js'))
});

gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: 'src'
        }
    });
});

gulp.task('watch', ['browser-sync', 'css'], function() {
    gulp.watch('src/sass/*.sass', ['css']);
    gulp.watch('src/*.html', browserSync.reload);
    gulp.watch('src/js/**/*.js', browserSync.reload);
});